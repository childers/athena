#include "TrigT2Tau/T2TauFinal.h"
#include "TrigT2Tau/T2TauEnergyTool.h"
#include "TrigT2Tau/T2TauEtFlowTool.h"
#include "TrigT2Tau/T2TauEtaPhiPtTool.h"
#include "TrigT2Tau/T2TauMatchingTool.h"
#include "TrigT2Tau/T2TauTrkRadiusTool.h"

DECLARE_COMPONENT( T2TauFinal )
DECLARE_COMPONENT( T2TauEnergyTool )
DECLARE_COMPONENT( T2TauEtFlowTool )
DECLARE_COMPONENT( T2TauEtaPhiPtTool )
DECLARE_COMPONENT( T2TauMatchingTool )
DECLARE_COMPONENT( T2TauTrkRadiusTool )

